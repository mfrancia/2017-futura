use northwind;

-- 66.  Individuare i prodotti il cui prezzo sia il più basso di tutti
select productname
from products
where unitprice <= all (select unitprice from products);
-- equivale a
select productname
from products
where unitprice = (select min(unitprice) from products);

-- 67.  Individuare le categorie di prodotti i cui prezzi siano i più bassi di tutti
select c.categoryname
from products p, categories c
where p.categoryid = c.categoryid
and unitprice <= all (select unitprice from products);

-- 68.  Individuare i prodotti il cui fatturato sia il più basso di tutti
select p.productid, productname, round(sum(od.unitprice*od.quantity),2)
from products p, `order details` od
where p.productid = od.productid
group by p.productid, productname
having round(sum(od.unitprice*od.quantity),2) <= all 
  (select round(sum(od1.unitprice*od1.quantity),2)
  from products p1, `order details` od1
  where p1.productid = od1.productid
  group by p1.productname);


-- oppure
select productname, round(sum(od.unitprice*od.quantity),2)
from products p, `order details` od
where p.productid = od.productid
group by productname
order by 2
limit 1;

-- 69.  Individuare i prodotti che non sono mai stati ordinati
select productname
from products p1
where not exists (select * from `order details` od where od.productid = p1.productid);
-- equivale a
select productid, productname
from products p1
where productid not in (select productid from `order details`);
-- oppure
select productname
from products p1 left outer join `order details` od on (od.productid = p1.productid)
where od.productid is null;

-- 70.  Individuare i prodotti che non sono mai stati ordinati da un cliente di nazionalità italiana
select productname
from products p1
where not exists (select *
          from `order details` od, orders o, customers c
          where od.productid = p1.productid and od.orderid = o.orderid
          and o.customerid = c.customerid and c.country = 'Italy');
-- equivale a
select distinct productname, p.productid
from products p, `order details` od, orders o
where od.productid = p.productid and od.orderid = o.orderid
and o.customerid not in (select customerid from customers where country = 'Italy');

-- 71.  Individuare i clienti che non hanno mai effettuato ordini
select contactname
from customers c
where not exists (select *
          from orders o
          where o.customerid = c.customerid);
-- equivale a
select contactname
from customers c
where customerid not in (select customerid from orders o);

-- 72.  Individuare i clienti che hanno effettuato ordini, ma mai di un prodotto di categoria Beverages
select contactname
from customers c
where not exists (select *
          from orders o, `order details` od, products p, categories c
          where o.customerid = c.customerid and o.orderid = od.orderid
          and od.productid = p.productid and p.categoryid = c.categoryid
          and c.categoryname = 'Beverages')
and exists (select *
      from orders o
      where o.customerid = c.customerid);
-- Attenzione! La seguente query include anche i clienti che non hanno fatto ordini!
select distinct contactname
from customers c
where customerid not in (select o.customerid
          from orders o, `order details` od, products p, categories cat
          where o.orderid = od.orderid and od.productid = p.productid 
                  and p.categoryid = cat.categoryid and cat.categoryname = 'Beverages');
-- Dobbiamo modificarla... Considerare tutti i clienti che non hanno comprato bevande non va bene
-- in quanto questo insieme include anche i clienti che non hanno mai fatto ordine. Dobbiamo 
-- assicurarci che i clienti abbiano fatto almeno un ordine, come? Aggiungendo un'altra condizione
-- nella clausola where
select distinct contactname
from customers c
where customerid not in (select o.customerid -- clienti che hanno comprato Bevande
          from orders o, `order details` od, products p, categories cat
          where o.orderid = od.orderid and od.productid = p.productid 
                  and p.categoryid = cat.categoryid and cat.categoryname = 'Beverages')
     and customerid in (select customerid from orders); -- clienti che hanno fatto almeno un ordine

-- 73.  Selezionare gli impiegati il cui fatturato è il più alto di tutti
select e.employeeid, firstname, lastname, count(*)
from employees e, orders o
where e.employeeid = o.employeeid
group by e.employeeid, firstname, lastname
having count(*) <= all 
  (select count(*)
  from employees e1, orders o1
  where e1.employeeid = o1.employeeid
  group by e1.firstname, e1.lastname);

-- 73 bis. Selezionare i responsabili degli impiegati il cui fatturato è il più alto di tutti
select e3.firstname, e3.lastname
from employees e3, 
  (select e.employeeid, reportsto
  from employees e, orders o
  where e.employeeid = o.employeeid
  group by e.employeeid, reportsto
  having count(*) <= all 
    (select count(*)
    from employees e1, orders o1
    where e1.employeeid = o1.employeeid
    group by e1.firstname, e1.lastname )
  ) e2
where e3.employeeid = e2.reportsto;

-- 74.  Selezionare il numero di ordini in cui compare almeno un prodotto di categoria Beverages
select count(*)
from orders o
where exists (select *
        from `order details` od, products p, categories c
        where o.orderid = od.orderid and od.productid = p.productid
        and p.categoryid = c.categoryid and c.categoryname = 'Beverages');
-- equivale a
select count(*)
from orders
where orderid in (select od.orderid
    from `order details` od, products p, categories c
    where od.productid = p.productid
    and p.categoryid = c.categoryid and c.categoryname = 'Beverages');
-- e gli ordini che non lo contengono?
select count(*)
from orders
where orderid not in (select od.orderid
    from `order details` od, products p, categories c
    where od.productid = p.productid
    and p.categoryid = c.categoryid and c.categoryname = 'Beverages');

-- 75.  Calcolare il fatturato medio dei prodotti che non sono mai stati venduti a clienti di Torino
select p.productname, round(sum(od.unitprice*od.quantity),2)
from products p, `order details` od
where p.productid = od.productid
and p.productid not in (select od1.productid
            from `order details` od1, orders o, customers c
            where od1.orderid = o.orderid and o.customerid = c.customerid
            and c.city = 'Torino')
group by p.productname;

-- 76.  Selezionare i prodotti che non compaiono in ordini spediti tra gennaio e marzo 1998 (compresi)
select p.productname
from products p
where p.productid not in (select od1.productid
            from `order details` od1, orders o
            where od1.orderid = o.orderid 
            and o.shippeddate between '1998-01-01' and '1998-03-31');