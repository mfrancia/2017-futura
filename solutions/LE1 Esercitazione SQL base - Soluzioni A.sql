# =====================
# 			A
# =====================
use northwind;

# 1
select *
from employees;

# 2
# Uguale...

# 3
select FirstName, LastName, Title
from employees;

# 4
select CompanyName, ContactName, City
from customers;

# 5
select *
from employees
order by birthdate; -- ASC di default

# 6
select *
from employees
order by hiredate desc;

# 7
select *
from products
order by productname desc, unitprice desc;

# 8
select *
from products
order by unitprice desc, productname desc; 

# 9
select *
from products
order by UnitPrice desc
limit 20;

# 10
select *
from products
where ProductName like 'S%';

# 11
select *
from products
where productname like 'S%e';

# 12
select *
from products
where productname not like 'S%';

# 13
select *
from products
where productname like '%S%';

# 14
select *
from products
where productname not like '%e' 
	and productname like 'S%';
    
# 15
# Note that both this and the next solution allows CompanyNames that
# start with any lower case letter (in the ASCII table, lower case letters come
# after capital ones).
select *
from suppliers
where companyname >= 'F';

# 15
select *
from suppliers
where companyname > 'E' and
	companyname not like 'E%';

# 16
select *
from employees
where birthdate between '1960-01-01' and '1963-12-01';
-- where birthdate between 19600101 and 19631201;

# 17
select *
from customers
where city in ('Strasbourg', 'London', 'Berlin');

# 18
select *
from orders
where ShipRegion is not null;
-- where not isnull(ShipRegion);


# 19
select *
from customers
where city not in ('Strasbourg', 'London', 'Berlin');
-- where city <> 'Strasbourg' and city <> 'London' and city <> 'Berlin';

# 20
select *
from orders
where Freight > 60 and
	(ShipRegion = 'Tchira' or ShipRegion is null);