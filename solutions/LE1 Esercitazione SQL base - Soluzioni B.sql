# =====================
# 			B
# =====================

# 21
select distinct CompanyName
from customers;

# 22
select distinct City
from customers;

# 23
select distinct ShipCity
from orders;

# 24
select City
from customers
union
select ShipCity
from orders;

# 25
select distinct ShipCity
from orders, customers
where ShipCity = City;

# 26
select orderid, c.*
from orders o, customers c
where o.CustomerID = c.CustomerID;
-- Soluzione equivalente con inner join
select orderid, c.*
from orders o inner join customers c on o.CustomerID = c.CustomerID;

# 27
select *
from orders
where CustomerID = 'ANTON';

# 28
select *
from orders o, customers c, `order details` od
where o.CustomerID = c.CustomerID
and od.OrderID = o.OrderID;

# 29
select *
from orders o, `order details` od, products p
where od.OrderID = o.OrderID
and od.ProductID = p.ProductID;
-- oppure usando 2 inner join (sono soluzioni equivalenti)
select * 
from orders o inner join `order details` od on od.OrderID = o.OrderID 
      inner join products p on od.ProductID = p.ProductID;

# 30
select *
from products p, suppliers s
where p.SupplierID = s.SupplierID and Country = 'UK';


# 31
select e2.FirstName
from employees e1, employees e2
where e1.ReportsTo = e2.EmployeeID and
	e1.FirstName = 'Robert' and e1.LastName = 'King';
-- 31 alt -- with subquery. Pay attention to the Query costs 
-- Query -> Explain current statement
select firstname, lastName
from employees
where employeeid = (
    select reportsto 
    from employees
    where firstname = 'Robert' and lastname = 'King'
);

# 32
select *
from employees
where ReportsTo is null;

# 33
select e1.employeeid, e1.firstname, e1.lastname, e1.reportsto, e2.employeeid, e2.firstname, e2.lastname 
from employees e1
left outer join employees e2
	on e1.ReportsTo = e2.EmployeeID;

-- ... con l'inner join?
select e1.employeeid, e1.firstname, e1.lastname, e1.reportsto, e2.employeeid, e2.firstname, e2.lastname 
from employees e1
inner join employees e2
	on e1.ReportsTo = e2.EmployeeID;

-- ... con il right outer join?
select e1.employeeid, e1.firstname, e1.lastname, e1.reportsto, e2.employeeid, e2.firstname, e2.lastname 
from employees e1
right join employees e2
	on e1.ReportsTo = e2.EmployeeID;

# 34
-- Il full outer join si ottiene unendo left e right outer join
select e1.employeeid, e1.firstname, e1.lastname, e1.reportsto, e2.employeeid, e2.firstname, e2.lastname 
from employees e1
left outer join employees e2
	on e1.ReportsTo = e2.EmployeeID
union
select e1.employeeid, e1.firstname, e1.lastname, e1.reportsto, e2.employeeid, e2.firstname, e2.lastname 
from employees e1
right outer join employees e2
	on e1.ReportsTo = e2.EmployeeID;
    
# 35
select distinct c.CompanyName
from customers c, orders o, `order details` od, products p
where c.CustomerID = o.CustomerID and
	o.OrderID = od.OrderID and
	od.ProductID = p.ProductID and
    p.ProductName = 'Aniseed Syrup';
