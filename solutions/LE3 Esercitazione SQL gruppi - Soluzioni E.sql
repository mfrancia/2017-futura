use northwind;

-- 49.  Contare il numero di prodotti presenti nella tabella Products
select count(*) from products;

-- 50.  Contare il numero distinto di categorie presenti nella tabella Products
select count(distinct categoryid) from products;

-- 51.  Individuare il prezzo maggiore nella tabella Products
select max(unitprice) from products;

-- 52.  Contare, per ogni id di categoria, il numero di prodotti presenti
select categoryid, count(*) from products group by categoryid;
-- equivale a
select categoryid, count(productid) from products group by categoryid;

-- 53.  Contare, per ogni nome di categoria, il numero di prodotti presenti e il prezzo medio degli stessi
select categoryname, count(*), round(avg(unitprice),2)
  from products p, categories c 
  where p.categoryid = c.categoryid
  group by categoryname;
  
-- 54.  Come la query precedente, ma mostrare solamente le categorie che hanno almeno 10 prodotti 
select categoryname, count(*), round(avg(unitprice),2)
  from products p, categories c 
  where p.categoryid = c.categoryid
  group by categoryname
  having count(*) >= 10;

-- 55.  Individuare, per ogni cliente, il numero totale di ordini effettuati
select companyname, count(*) 
  from customers c, orders o
  where c.customerid = o.customerid
  group by companyname;

-- 56.  Come la query precedente, ma non considerando gli ordini che devono essere ancora spediti (considera il campo ShippedDate)
select companyname, count(*) 
  from customers c, orders o
  where c.customerid = o.customerid and o.shippeddate is not null
  group by companyname;

-- 57.  Contare, per ogni dipendente, il numero totale di ordini gestiti  
select e.employeeid, firstname, lastname, count(*) 
  from employees e, orders o
  where e.employeeid = o.employeeid
  group by e.employeeid, firstname, lastname;

-- 58.  Contare, per ogni nazione dei dipendenti, il numero totale di ordini gestiti  
select country, count(*) 
  from employees e, orders o
  where e.employeeid = o.employeeid
  group by country;

-- 59.  Contare, per ogni città dei dipendenti statunitensi, il numero totale di ordini gestiti
select city, count(*) 
  from employees e, orders o
  where e.employeeid = o.employeeid and country = 'USA'
  group by city;

-- 60.  Calcolare, per ogni ordine, il fatturato totale
select od.orderid, round(sum(unitprice*quantity),2) 
  from `order details` od
  group by od.orderid;
  
-- 61.  Calcolare, per ogni impiegato, il fatturato totale 
select e.employeeid, firstname, lastname, round(sum(unitprice*quantity),2) 
  from employees e, orders o, `order details` od
  where e.employeeid = o.employeeid and o.orderid = od.orderid
  group by e.employeeid, firstname, lastname;
-- equivale a
select e.employeeid, firstname, lastname, round(sum(unitprice*quantity),2) 
  from (employees e join orders o on e.employeeid = o.employeeid) 
    join `order details` od on o.orderid = od.orderid
  group by e.employeeid, firstname, lastname;
  
-- 62.  Calcolare, per ogni impiegato, il fatturato totale generato dai suoi sottoposti
select e1.firstname, e1.lastname, round(sum(unitprice*quantity),2) 
  from employees e1, employees e2, orders o, `order details` od
  where e1.employeeid = e2.reportsto and e2.employeeid = o.employeeid 
      and o.orderid = od.orderid
  group by e1.firstname, e1.lastname;

-- 63.  Calcolare, per ogni prodotto, il numero totale di unità vendute
select productname, sum(od.quantity)
  from products p, `order details` od
  where p.productid = od.productid
  group by productname;
  
-- 64.  Calcolare, per ogni prodotto e per ogni anno (su OrderDate), il numero totale di unità vendute
select productname, year(orderdate), sum(od.quantity)
  from products p, `order details` od, orders o
  where p.productid = od.productid and od.orderid = o.orderid
  group by productname, year(orderdate);
  
-- 65.  Come la query precedente, ma considerando solo gli ordini spediti dalla compagnia Speedy Express
select productname, year(orderdate), sum(od.quantity)
  from products p, `order details` od, orders o, shippers s
  where p.productid = od.productid and od.orderid = o.orderid 
  and o.shipvia = s.shipperid and s.companyname = 'Speedy Express'
  group by productname, year(orderdate);