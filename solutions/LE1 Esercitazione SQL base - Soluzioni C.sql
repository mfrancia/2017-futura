# =====================
#       C
# =====================

# 36
select concat(FirstName, LastName) NameSurname
from employees;

# 37
select concat(FirstName, LastName) NameSurname, char_length(concat(FirstName, LastName))
from employees;

# 38
select concat(FirstName, '-', LastName) NameSurname
from employees;

# 39
select timestampdiff(year, birthdate, now()) Age
from employees;

# 40
select FirstName, dayname(birthdate) DName
from employees;

# 41
select concat(substring(FirstName, 1, 1), substring(LastName, 1, 1))
from employees;

# 42
select pow(std(Freight), 2)
from orders;

# 43
select timestampdiff(day, ShippedDate, RequiredDate)
from orders;

# 44
select *
from orders
order by rand();
