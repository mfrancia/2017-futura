create database esercizi;

----------------------

-- 45 CREATE
create table 1impiegati (
	CodImp int primary key,
	NomeImp varchar(30) not null,
	Qualifica varchar(20) not null
);

create table 1progetti (
	CodProg int primary key,
	NomeProg varchar(20) not null
);

create table 1collabora (
	CodImp int,
	CodProg int,
	MesiUomo decimal(3,1),
	primary key (CodImp,CodProg),
	foreign key (CodImp) references 1impiegati (CodImp),
	foreign key (CodProg) references 1progetti (CodProg)
);

create table 3atleti (
	CodA int primary key,
	Nome varchar(30) not null,
	Nazione varchar(20),
	DataNascita date,
	Score int default 0
);

create table 3gare (
	CodG int primary key,
	Nome varchar(30) not null,
	Disciplina varchar(20) not null,
	DataOra datetime not null,
	Luogo varchar(20) not null,
	Giudice varchar(30) not null
);

create table 3partecipazioni (
	Atleta int,
	Gara int,
	Risultato time not null,
	PosArrivo int not null,
	primary key (Atleta,Gara),
	foreign key (Atleta) references 3atleti (CodA),
	foreign key (Gara) references 3gare (CodG)
);

create table 3testdoping (
	Atleta int,
	Data date,
	Test varchar(1),
	Risultato boolean not null,
	primary key (Atleta,Data,Test),
	foreign key (Atleta) references 3atleti (CodA)
);

-- 46 INSERT
insert into 1impiegati values (1, 'Andrea', 'Programmatore'),(2, 'Bianca', 'Analista'),(3, 'Carlo', 'Analista');
insert into 1progetti values (1, 'ProgA'),(2, 'ProgB');
insert into 1collabora values (1, 1, 0),(2, 1, 0),(1, 2, 0),(3, 2, 0);

insert into 3atleti (CodA, Nome, Nazione) values (4, 'Davide', 'Italia'),(5, 'Enrico', 'Francia');
insert into 3gare (CodG, Nome, Disciplina, DataOra, Luogo, Giudice) values 
	(1, 'Gara1', 'DiscA', '2015-01-10 15:00:00', 'Bologna', 'Giacomo'),
	(2, 'Gara2', 'DiscB', '2015-01-20 15:00:00', 'Bologna', 'Irene'),
	(3, 'Gara3', 'DiscA', '2015-02-10 15:00:00', 'Cesena', 'Irene'),
	(4, 'Gara4', 'DiscB', '2015-02-20 15:00:00', 'Cesena', 'Giacomo');
insert into 3partecipazioni (Atleta, Gara, Risultato, PosArrivo) values 
	(4, 1, '01:02:03', 1),(4, 2, '01:02:03', 2),(4, 3, '01:02:03', 1),(4, 4, '01:02:03', 2),
	(5, 1, '01:02:03', 3),(5, 2, '01:02:03', 1),(5, 3, '01:02:03', 4),(5, 4, '01:02:03', 1);
insert into 3testdoping (Atleta, Data, Test, Risultato) values 
	(4, '2015-01-31', 'A', true), (4, '2015-02-28', 'B', false),
	(5, '2015-01-31', 'A', false), (5, '2015-02-28', 'B', true);


-- 47 UPDATE
-- 47 a i. Dato il codice di un progetto, modificarne il nome
update 1progetti 
set NomeProg = 'Newname'
where CodProg = 1;

-- 47 a ii.	Data una qualifica, incrementare di 1 l’ammontare dei MesiUomo di tutti gli impiegati su tutti i progetti
update 1collabora c, 1impiegati i
set c.MesiUomo = c.MesiUomo + 1
where c.CodImp = i.CodImp 
and i.Qualifica = 'Programmatore';

-- 47 a iii. Data una qualifica e il nome di un progetto, incrementare di 2 l’ammontare dei MesiUomo di tutti gli impiegati che lavorano al dato progetto
update 1collabora c, 1impiegati i, 1progetti p
set c.MesiUomo = c.MesiUomo + 2
where c.CodImp = i.CodImp and c.CodProg = p.CodProg
and i.Qualifica = 'Programmatore' and p.NomeProg = 'Newname';

-- 47 b i. Imposta a negativi tutti i test antidoping svolti da atleti italiani
update 3testdoping t, 3atleti a 
set t.risultato = false 
where t.atleta = a.coda and a.nazione = 'Italia';
-- oppure
update 3testdoping t 
set t.risultato = 0 
where t.atleta in (select a.coda 
					from 3atleti a
					where a.nazione = 'Italia');

-- 47 b ii. Imposta lo score di un atleta come la somma delle posizioni alle gare disputate nell’anno in corso					
update 3atleti a 
set a.score = (select sum(p.posarrivo) 
				from 3partecipazioni p 
				where p.atleta = a.coda);
			
-- 47 b iii. Squalificare (PosArrivo = 100) gli atleti in tutte le partecipazioni degli atleti che, nello stesso mese della gara, hanno effettuato un testdoping positivo			
update 3partecipazioni p, 3testdoping t, 3gare g
set p.posarrivo = 100
where p.atleta = t.atleta and p.gara = g.codg
and month(g.dataora) = month(t.data) and year(g.dataora) = year(t.data)
and t.risultato = true;

-- 48 DELETE
-- 48 a i. Cancellare tutte le collaborazioni che non superano i 5 mesi uomo
delete from 1collabora where mesiuomo <= 5;

-- 48 a ii. Cancellare tutte le collaborazioni di impiegati con una determinata qualifica
delete from 1collabora where codimp in (select codimp from 1impiegati where qualifica = 'Programmatore');

-- 48 a iii. Cancellare i progetti per cui non esistono collaborazioni
delete from 1progetti where codprog not in (select codprog from 1collabora);

-- 48 b i. Cancellare tutte le partecipazioni in cui PosArrivo = 100
delete from 3partecipazioni where posarrivo = 100;

-- 48 b ii. Cancellare tutte le gare di febbraio
delete from 3gare where month(data)=2; -- Errore: cancellare prima le partecipazioni!
delete from 3partecipazioni where gara in (select codgara from 3gare where month(data)=2);

-- 48 b iii. Cancellare tutti i testdoping svolti da atleti che hanno partecipato a gare di una determinata disciplina
delete from 3testdoping where atleta in (select atleta from 3partecipazioni p, 3gare g where p.gara = g.codg and disciplina = 'DiscA');
