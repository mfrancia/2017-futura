﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfMySQL
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            loadData();
        }

        private void loadData()
        {
            MySqlConnection conn = new MySqlConnection("server=localhost;database=mydb;uid=root;password=123456789");
            try
            {
                conn.Open();
                String query = "select * from imp";
                MySqlCommand qrycmd = new MySqlCommand(query, conn);
                // MySqlDataReader reader = qrycmd.ExecuteReader();
                // while (reader.Read())
                // {
                // Console.WriteLine(reader["codimp"] + " " + reader["nome"]);
                // }
                MySqlDataAdapter adp = new MySqlDataAdapter(qrycmd);
                DataSet ds = new DataSet();
                adp.Fill(ds, "LoadDataBinding");
                dgEmployees.DataContext = ds;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            insertinto();
            loadData();
        }

        private void insertinto()
        {
            MySqlConnection conn = new MySqlConnection("server=localhost;database=mydb;uid=root;password=123456789");
            try
            {
                conn.Open();
                String insert = "insert into imp(codimp, nome, ruolo) values(@codimp, @nome, @ruolo)";
                MySqlCommand cmd = new MySqlCommand(insert, conn);
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@codimp", txtCodimp.Text);
                cmd.Parameters.AddWithValue("@nome", txtNome.Text);
                cmd.Parameters.AddWithValue("@ruolo", txtRuolo.Text);
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
    }
}
