<html>
<body>
<form action="insert.php" method="post">
    <p>
        <label for="codimp">CodImp:</label>
        <input type="text" name="codimp" id="codimp">
    </p>

    <p>
        <label for="nome">Nome:</label>
        <input type="text" name="nome" id="nome">
    </p>

    <p>
        <label for="ruolo">Ruolo:</label>
        <input type="text" name="ruolo" id="ruolo">
    </p>

    <input type="submit" value="Submit">
</form>

Lista degli impiegati:</br>

<?php
include('credentials.php');
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

function doSomething($conn)
{
    $sql = "select codimp, nome, ruolo from imp";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while ($row = mysqli_fetch_assoc($result)) {
            echo "id: " . $row["codimp"] . " - Name: " . $row["nome"] . " - Ruolo: " . $row["ruolo"] . "</br>";
        }
    } else {
        echo "0 results";
    }
}

doSomething($conn);
mysqli_close($conn);
?>
</body>
</html>