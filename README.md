# Modulo Basi di Dati

Settembre, 2017. 30 ore.

## `Whoami`?

**Matteo Francia** [`m.francia[at]unibo.it`]

## Programma

- Introduzione teorica
    - Sistemi informativi, basi di dati e DBMS
    - Modello relazionale
    - Modello ER
- Gestione delle basi di dati
    - DML, DDL
    - SQL base, raggruppamenti e subquery
- Valutazione (teorica e pratica)

|           | Settembre 1 [8 ore]              | Settembre 7 [8 ore]             | Settembre 13 [8 ore]   | Settembre 22 [6 ore] |
|-----------|--------------------------------------|-------------------------------------|----------------------------|--------------------------|
| Mattina   | Introduction to IS [T1], Relational Model [T2, TE1] | ER model and comprehension [T4, TE2, TE3]      | SQL group by [L3], SQL subquery [L4] | Test e correzione                |
| Pomeriggio | MySQL [L1], SQL: DML [L2 to p.28], Lab SQL [A]    | SQL: DDL [L2 from p.28], Lab SQL [B, D]  | Lab SQL [C, E, F]  | PHP, C# e MySQL                      |

## Requisiti

##### Teorici

- Qualche nozione insiemistica

##### Tecnici

0. Installare [MySQL Server](https://dev.mysql.com/downloads/mysql/) e [MySQL Workbench](https://dev.mysql.com/downloads/workbench/)
    0. Creare l'utente `root` con password `123456789`
1. Caricare [Northwind database](https://northwinddatabase.codeplex.com/) in MySQL Server
    0. Copiare Northwind from `data/`
    1. Aprire MySQL Workbench
    2. Connetersi a MySQL Server come utente `root`
    3. Importare Northwind.sql: `File -> Open SQL Script`
    4. Eseguire lo script
    5. Eseguire una query di prova (e.g. `select * from customers`)
2. Importare un qualsiasi script `*.sql
    0. Importare `script.sql`: `File -> Open SQL Script`

### Conosci git?

No? [**Questo tutorial online**](https://try.github.io/levels/1/challenges/1) potrebbe esserti utile! :)

## Materiale: https://bitbucket.org/mfrancia/2017-futura-origin

## Esercizi

Controlla le soluzioni in `solutions/`.

### A
1. Visualizzare tutti i dipendenti.
2. Ripetere Query 1 sulle altre tabelle per familiarizzare con lo schema Northwind.
3. Visualizzare FirstName, LastName e Title di tutti i dipendenti.
4. Visualizzare CompanyName, ContactName e City di tutti i clienti.
5. Visualizzare tutti i dipendenti in ordine ascendente di BirthDate.
6. Visualizzare tutti i dipendenti in ordine discendente di HireDate.
7. Visualizzare tutti i prodotti in ordine discendente di ProductName e UnitPrice.
8. Visualizzare tutti i prodotti in ordine discendente di UnitPrice e ProductName.
9. Visualizzare i primi 20 prodotti ordinati in ordine discendente per UnitPrice.
10. Visualizzare tutti i prodotti il cui ProductName inizia per S.
11. Visualizzare tutti i prodotti il cui ProductName inizia per S e termina con e.
12. Visualizzare tutti i prodotti il cui ProductName non inizia per S.
13. Visualizzare tutti i prodotti il cui ProductName contiene almeno una S.
14. Visualizzare tutti i prodotti il cui ProductName inizia per S ma non termina per e.
15. Visualizzare tutti fornitori il cui CompanyName inizia con un carattere successivo a E.
16. Visualizzare tutti i dipendenti nati tra il 1960-01-01 e il 1963-12-01.
17. Visualizzare tutti i clienti la cui City è Strasbourg, London o Berlin.
18. Visualizzare tutti gli ordini che hanno una ShipRegion.
19. Visualizzare tutti i clienti la cui City non è Strasbourg, London o Berlin.
20. Visualizzare tutti gli ordini con valore di Freight superiore a 60 e ShipRegion uguale a Tchira o non specificata.
### B
21. Visualizzare l’elenco di CompanyName (senza ripetizioni) di tutti i clienti.
22. Visualizzare l’elenco (senza ripetizioni) delle città (City) presenti in Customers.
23. Visualizzare l’elenco (senza ripetizioni) delle città (ShipCity) presenti in Orders.
24. Visualizzare l’elenco (senza ripetizioni) delle città presenti in Customers o in Orders. (Hint: l'unione delle città di Customers e Orders.)
25. Visualizzare l’elenco (senza ripetizioni) delle città presenti sia in Customers che in Orders. (NB: MySQL non supporta l’operatore di intersezione.)
26. Visualizzare tutti gli ordini con i relativi dati dei clienti che li hanno effettuati.
27. Visualizzare tutti gli ordini del cliente con ID ANTON.
28. Visualizzare tutti i dettagli di tutti gli ordini includendo anche le informazioni dei relativi clienti.
29. Visualizzare tutti i dettagli di tutti gli ordini includendo anche le informazioni dei relativi prodotti.
30. Visualizzare tutti i prodotti il cui fornitore ha sede in UK.
31. Visualizzare il nome del dipendente a cui fa rapporto King Robert.
32. Visualizzare tutti i dipendenti che non fanno rapporto a nessuno.
33. Visualizzare tutti i dipendenti specificando, dove possibile, a chi fanno rapporto.
34. Visualizzare tutti i dipendenti specificando, dove possibile, a chi fanno rapporto, inoltre, visualizzare anche tutti quei dipendenti a cui nessuno fa rapporto. (NB: MySQL non supporto l’operatore FULL OUTER JOIN.)
35. Visualizzare tutti i clienti (senza ripetizioni) che hanno acquistato almeno una volta un dato prodotto.
### C
36. Visualizzare la concatenazione di nome e cognome di tutti i dipendenti.
37. Visualizzare la concatenazione di nome e cognome di tutti i dipendenti e la lunghezza di tale
concatenazione.
38. Visualizzare, per tutti i dipendenti, nome e cognome uniti dal carattere -.
39. Visualizzare l’età di tutti i dipendenti. (Hint: la funzione timestampdiff(year, date1, date2)
restituisce la differenza tra date2 e date1).
40. Per ogni dipendente, visualizzare il nome del giorno della settimana in cui è nato.
41. Visualizzare le iniziali (in un’unica colonna) di tutti i dipendenti.
42. Visualizzare la varianza dei valori di Freight di tutti gli ordini. (Hint: la varianza è il quadrato della
deviazione standard.)
43. Per ogni ordine, visualizzare i giorni di ritardo (ShippedDate - RequiredDate).
44. Visualizzare tutti gli ordini in ordine casuale. (Hint: la funzione rand() può essere usata anche
nell’ordinamento)
### D
45. Dati gli schemi dell’esercitazione relativa al modello relazionale (TE1), costruire le corrispondenti tabelle
prestando attenzione a definire i corretti vincoli di chiave e integrità referenziale. Utilizzare tipi di
dato (ragionevolmente) realistici.
46. Popolare (tramite INSERT) con dati (fittizi) le tabelle create al punto precedente.
47. Modificare (tramite UPDATE) alcune tuple delle tabelle popolate al punto precedente.
### E
49. Contare il numero di prodotti presenti nella tabella Products
50. Contare il numero distinto di categorie presenti nella tabella Products
51. Individuare il prezzo maggiore nella tabella Products
52. Contare, per ogni id di categoria, il numero di prodotti presenti
53. Contare, per ogni nome di categoria, il numero di prodotti presenti e il prezzo medio degli stessi
54. Come la query precedente, ma mostrare solamente le categorie che hanno almeno 10 prodotti
55. Individuare, per ogni cliente, il numero totale di ordini effettuati
56. Come la query precedente, ma non considerando gli ordini che devono essere ancora spediti (considera il campo ShippedDate)
57. Contare, per ogni dipendente, il numero totale di ordini gestiti
58. Contare, per ogni nazione dei dipendenti, il numero totale di ordini gestiti
59. Contare, per ogni città dei dipendenti statunitensi, il numero totale di ordini gestiti
60. Calcolare, per ogni ordine, il fatturato totale
61. Calcolare, per ogni impiegato, il fatturato totale
62. Calcolare, per ogni impiegato, il fatturato totale generato dai suoi sottoposti
63. Calcolare, per ogni prodotto, il numero totale di unità vendute
64. Calcolare, per ogni prodotto e per ogni anno (su OrderDate), il numero totale di unità vendute
65. Come la query precedente, ma considerando solo gli ordini spediti dalla compagnia Speedy Express
### F
66. Individuare i prodotti il cui prezzo sia il più basso di tutti
67. Individuare le categorie di prodotti i cui prezzi siano i più bassi di tutti
68. Individuare i prodotti il cui fatturato sia il più basso di tutti
69. Individuare i prodotti che non sono mai stati ordinati
70. Individuare i prodotti che non sono mai stati ordinati da un cliente di nazionalità italiana
71. Individuare i clienti che non hanno mai effettuato ordini
72. Individuare i clienti che hanno effettuato ordini, ma mai di un prodotto di categoria Beverages
73. Selezionare gli impiegati il cui fatturato è il più alto di tutti
74. Selezionare i responsabili degli impiegati il cui fatturato è il più alto di tutti
75. Selezionare il numero di ordini in cui compare almeno un prodotto di categoria Beverages
76. Calcolare il fatturato medio dei prodotti che non sono mai stati venduti a clienti di Torino
77. Selezionare i prodotti che non compaiono in ordini spediti tra gennaio e marzo 1998 (compresi)
