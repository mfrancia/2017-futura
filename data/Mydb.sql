# ---------------------------------------------------------------------- #
# Author:                Matteo Francia                                  #
# ---------------------------------------------------------------------- #

# ---------------------------------------------------------------------- #
# DDL: DATABASE CREATION AND SCHEMA DEFINITION                           #
# ---------------------------------------------------------------------- #
drop database if exists mydb;
create database if not exists mydb;
use mydb;
create table Sedi(
    Sede char(3) primary key,
    Responsabile varchar(20) default 'Unknown',
    Citta varchar(20) not null
);
create table Imp (
   CodImp char(4) primary key,
   Nome varchar(20) not null,
   Sede char(3) references sedi(sede),
   Ruolo varchar(20) default 'Programmatore',
   Stipendio int check (stipendio > 0)
);
create table ImpWithNull ( -- same as Imp but filled with NULL values
   CodImp char(4) primary key,
   Nome varchar(20) not null,
   Sede char(3) references sedi(sede),
   Ruolo varchar(20) default 'Programmatore',
   Stipendio int check (stipendio > 0)
);
create table Prog(
    CodProg char(3) not null,
    Citta varchar(20) not null
);
create table R (
    a integer,
    b char(1)
);
create table S (
    c integer,
    b char(1)
);
create table Famiglia(
    Genitore varchar(20),
    Figlio varchar(20)
);
# ---------------------------------------------------------------------- #
# DML: FILL OUT DATABASE                                                 #
# ---------------------------------------------------------------------- #
insert into Sedi values ('S01', 'Biondi', 'Milano'), 
						('S02', 'Mori', 'Bologna'),
						('S03', 'Fulvi', 'Milano');
insert into Prog values ('P01', 'Milano'), 
						('P01', 'Milano'),
						('P02', 'Bologna');
insert into Imp values ('E001', 'Rossi', 'S01', 'Analista', 2000),
					   ('E002', 'Verdi', 'S02', 'Sistemista', 1500),
					   ('E003', 'Bianchi', 'S01', 'Programmatore', 1000),
					   ('E004', 'Gialli', 'S03', 'Programmatore', 1000),
					   ('E005', 'Neri', 'S02', 'Analista', 2500),
					   ('E006', 'Grigi', 'S01', 'Sistemista', 1100),
					   ('E007', 'Violetti', 'S01', 'Programmatore', 1000),
					   ('E008', 'Aranci', 'S02', 'Programmatore', 1200);
insert into ImpWithNull values ('E001', 'Rossi', 'S01', 'Analista', 2000),
				               ('E002', 'Verdi', 'S02', 'Sistemista', 1500),
					           ('E003', 'Bianchi', 'S01', 'Programmatore', 1000),
					           ('E004', 'Gialli', 'S03', 'Programmatore', null),
					           ('E005', 'Neri', 'S02', 'Analista', 2500),
					           ('E006', 'Grigi', 'S01', 'Sistemista', null),
					           ('E007', 'Violetti', 'S01', 'Programmatore', 1000),
					           ('E008', 'Aranci', 'S02', 'Programmatore', 1200);
insert into R values (1, 'a'), (1, 'a'), (2, 'a'), (2, 'b'), (2, 'c'), (3, 'b');
insert into S values (1, 'a'), (1, 'b'), (2, 'a'), (2, 'c'), (3, 'c'), (4, 'd');
insert into Famiglia values ('Luca', 'Anna'), ('Maria', 'Anna'), ('Giorgio', 'Luca'),
						    ('Silvia', 'Maria'), ('Enzo', 'Maria');