use mydb;

-- Un breve ripasso sul join
-- Sede S04 non ha impiegati
insert into sedi(sede, responsabile, citta) values('S04', 'Neri', 'Cesena');
-- L'impiegato E100 lavora in una sede non presente nel database, viola il vincolo di integrità referenziale?
insert into imp values ('E100', 'Pippo', 'S05', 'Programmatore', 100);
-- L'impiegato E101 non ha sede, viola il vincolo di integrità referenziale?
insert into imp values ('E101', 'Pluto', null, 'Programmatore', 100);

-- compaiono E100 e E101?
select i.codimp, s.responsabile
from imp i, sedi s
where i.sede = s.sede;

-- compaiono E100 e E101?
select i.codimp, s.responsabile
from imp i inner join sedi s on i.sede = s.sede;

select *
from imp i right join sedi s on i.sede = s.sede;

select *
from imp i left join sedi s on i.sede = s.sede;


-- Fino ad ora query su singole tuple
select ruolo from imp where codimp = 'E001';
select responsabile from sedi where sede = 'S02';

-- e se volessimo inforazioni aggregate?
-- Somma degli stipendi degli impiegati della sede S01
select sum(stipendio) as totstipS01
from imp 
where sede = 'S01';

-- Stipendio minimo degli impiegati della sede S01
select min(stipendio), max(stipendio), avg(stipendio)
from imp 
where sede = 'S01';

select sum(stipendio * 12) as totstipannuiS01
from imp 
where sede = 'S01';

select sum(distinct stipendio)
from imp 
where sede = 'S01';

-- Numero degli impiegati nella sede S01
select count(*) as numimpS01
from imp
where sede = 'S01';

-- Numero degli stipendi nella sede S01
select count(*) as numimpS01
from imp
where sede = 'S01';

-- Numero degli impiegati nella sede S01... i NULL sono contati?
select count(*) as numimpS01
from impwithnull
where sede = 'S01';

-- e se volessimo contare il numero di programmatori
-- PER OGNI sede? Dobbiamo PRIMA raggruppare per sede e POI
-- contare gli impiegati programmatori
select sede, count(*) as numprog
from imp
where ruolo = 'Programmatore'
group by sede;

-- Per ogni ruolo, lo stipendio medio nelle sedi di Milano
SELECT I.Ruolo, AVG(I.Stipendio) AS AvgStip
FROM Imp I JOIN Sedi S ON (I.Sede = S.Sede)
WHERE S.Citta =  'Milano'
GROUP BY I.Ruolo;

-- Per ogni sede di Milano, lo stipendio medio
SELECT I.Sede, AVG(I.Stipendio) AS AvgStip
FROM Imp I JOIN Sedi S ON (I.Sede = S.Sede)
WHERE S.Citta =  'Milano'
GROUP BY I.Sede;

-- Per ogni sede di Milano, lo stipendio medio
SELECT I.Sede, I.Ruolo, AVG(I.Stipendio) 
FROM Imp I JOIN Sedi S ON (I.Sede = S.Sede)
WHERE S.Citta =  'Milano'
GROUP BY I.Sede, I.Ruolo;

SELECT Sede
FROM Imp
GROUP BY Sede;
-- equivale a
SELECT DISTINCT Sede
FROM Imp;

SELECT Sede, COUNT(*) AS NumImp
FROM Imp
GROUP BY Sede
HAVING COUNT(*) > 2;

SELECT Sede, COUNT(*) AS NumImp
FROM Imp
GROUP BY Sede
HAVING Sede <> 'S01';
-- equivale a
SELECT Sede, COUNT(*) AS NumImp
FROM Imp
WHERE Sede <> 'S01'
GROUP BY sede;

-- la query al completo!
-- Per ogni sede di Bologna in cui il numero di impiegati è almeno 3, si 
-- vuole conoscere il valor medio degli stipendi, ordinando il risultato per 
-- valori decrescenti di stipendio medio e quindi per sede
SELECT I.Sede, AVG(Stipendio) AS AvgStipendio
FROM Imp I, Sedi S
WHERE I.Sede = S.Sede AND S.Citta =  'Bologna'
GROUP BY I.Sede
HAVING COUNT(*) >= 3
ORDER BY AvgStipendio DESC, Sede;