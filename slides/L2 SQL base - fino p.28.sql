# ---------------------------------------------------------------------- #
# DML: WORKING ON THE DATABASE                                           #
# ---------------------------------------------------------------------- #
use mydb;

select codimp, nome, ruolo
from imp
where sede = 'S01';

select codimp, nome, sede, ruolo, stipendio
from imp
where sede = 'S01';

select *
from imp
where sede = 'S01';

-- Same as: select * from imp where true;
select *
from imp;

select ruolo
from imp
where sede = 'S01';

select distinct ruolo
from imp
where sede = 'S01';

select codimp, stipendio * 12
from imp
where sede = 'S01';

select codimp as Codice , stipendio * 12 as StipendioAnnuo
from imp
where sede = 'S01';

select codimp Codice, stipendio * 12 StipendioAnnuo
from imp
where sede = 'S01';

select imp.codimp Codice, imp.stipendio * 12 StipendioAnnuo
from imp
where imp.sede = 'S01';

select I.codimp Codice, I.stipendio * 12 StipendioAnnuo
from imp I
where I.sede = 'S01';

select nome
from imp
where nome like '_i%i';

select nome
from imp
where nome like '%Bianchi';

select nome
from imp
where nome like '_Bianchi';

select nome, stipendio
from imp
where stipendio between 1300 and 2000;

select codimp, sede
from imp
where sede in ('S02', 'S03');

select codimp, sede
from imp
where sede = 'S02' or sede = 'S03';

select codimp
from impwithnull
where stipendio > 1500 or stipendio <= 1500;
-- NB (stipendio > 1500 and stipendio <= 1500) = TRUE!
-- However, this is not the same as
-- select codimp from impwithnull where true;

select codimp, sede, stipendio
from impwithnull
where stipendio > 1500 or sede = 'S03';

select codimp
from impwithnull
where stipendio is null;

select nome, stipendio
from imp 
order by stipendio desc, nome;

select a from r
union
select c as a from s;

select a from r
union
select c from s;

select a, b from r -- should return error
union
select b, c as a from s;

select b from r
union all 
select b from s;

-- MySQL doesn't have 'intersect' operator, we can write it using INNER JOIN
-- select b from r intersect select b from s;
select distinct r.b 
from r inner join s on r.b = s.b;
-- ... and 'except' neither
select b
from s
where b not in (
    select b from r
);