use mydb;

-- Un breve ripasso sul join
create table s2 (sede char(3));
create table i2 (codi char(3), sede char(3), ruolo char(1));
insert into s2 values ('s1');
insert into s2 values ('s2');
insert into i2 values ('i1', 's1', 'a');
insert into i2 values ('i2', 's2', 'p');

select * from s2 s join i2 i on s.sede = i.sede;
select * from s2 s join i2 i on s.sede = i.sede and i.ruolo = 'a';
select * from s2 s left join i2 i on s.sede = i.sede and i.ruolo = 'a';
select * from s2 s right join i2 i on s.sede = i.sede and i.ruolo = 'a';

-- impiegati delle sedi di Milano
SELECT CodImp
FROM Imp
WHERE Sede IN (
  SELECT   Sede
  FROM Sedi
  WHERE Citta = 'Milano'
);
-- equivale a
SELECT CodImp
FROM Imp
WHERE Sede IN ('S01', 'S03');

-- impiegati con stipendio minimo 
SELECT CodImp
FROM Imp
WHERE Stipendio = (
  SELECT MIN(Stipendio)
  FROM Imp
);

-- il responsabile della sede dell'impiegato E001
SELECT Responsabile
FROM   Sedi 
WHERE  Sede = (
  SELECT Sede 
  -- al massimo una sede
  FROM Imp
  WHERE CodImp = 'E001'
);

-- resp. delle sedi in cui esistono impiegati che 
-- guadagnano almeno 1500
SELECT Responsabile
FROM   Sedi 
WHERE  Sede = ANY (
  SELECT Sede
  FROM Imp
  WHERE  Stipendio > 1500
);

-- impiegati con stipendio minimo 
SELECT CodImp
FROM Imp
WHERE Stipendio <= ALL (
  SELECT Stipendio
  FROM Imp
);

SELECT CodImp
FROM Imp 
WHERE Sede IN (
  SELECT Sede
  -- sedi presenti in città in cui
  -- non è attivo il prog. P02
  FROM Sedi
  WHERE Citta NOT IN (
    SELECT Citta
    FROM Prog
    WHERE CodProg ='P02'
  )
);
-- queste due query sono differenti!
SELECT CodImp
FROM Imp 
WHERE Sede IN (
  SELECT Sede
  FROM Sedi, Prog
  WHERE Sedi.Citta <> Prog.Citta AND Prog.CodProg = 'P02'
);

-- sedi in cui esistono analisti? (nested)
SELECT Sede 
FROM   Sedi S
WHERE EXISTS (
  SELECT *
  FROM Imp
  WHERE Ruolo = 'Analista'
);

-- sedi in cui esistono analisti? (flat)
SELECT DISTINCT I.Sede
FROM Sedi S, Imp I
WHERE S.Sede = I.Sede AND I.Ruolo = 'Analista';

-- sedi senza analisti?
SELECT Sede
FROM   Sedi S
WHERE  NOT EXISTS (
  SELECT *
  FROM   Imp
  WHERE  Ruolo = 'Analista' AND  Sede = S.Sede
);

-- questa forma piatta restituisce le sedi in cui lavora almeno un impiegato con  ruolo diverso da un analista
SELECT DISTINCT I.Sede
FROM   Sedi S JOIN Imp I ON S.Sede = I.Sede
WHERE  I.Ruolo <> 'Analista';

-- questa e' corretta
SELECT DISTINCT *
FROM  Sedi S LEFT OUTER JOIN Imp I ON (S.Sede = I.Sede AND I.Ruolo = 'Analista')
WHERE I.CodImp IS NULL;

-- questa non e' corretta
SELECT DISTINCT S.Sede
FROM   Sedi S LEFT OUTER JOIN Imp I ON (S.Sede = I.Sede)
WHERE I.CodImp IS NULL AND I.Ruolo = 'Analista';

SELECT Sede, COUNT(CodImp) AS NumProg
FROM     Imp I1 
WHERE    I1.Ruolo = 'Programmatore' 
GROUP BY I1.Sede 
HAVING   COUNT(CodImp) >= ALL (
  SELECT COUNT(CodImp)
  FROM     Imp I2 
  WHERE    I2.Ruolo = 'Programmatore' 
  GROUP BY I2.Sede
);

-- Seleziona, per ogni Sede in cui è presente almeno un impiegato programmatore, 
-- lo stipendio medio di tutti i dipendenti. Nell'esempio, lo stipendio medio viene 
-- calcolato solo per i programmatori
SELECT Sede, AVG(Stipendio)
FROM   Imp
WHERE    Ruolo = 'Programmatore'
GROUP BY Sede;

SELECT DISTINCT I1.Sede, (
            SELECT AVG(I2.Stipendio)
            FROM Imp I2
            WHERE I2.Sede = I1.Sede
          )
FROM   Imp I1
WHERE  I1.Ruolo = 'Programmatore';

SELECT Sede, AVG(Stipendio)
FROM   Imp
WHERE  Sede IN (
  SELECT DISTINCT Sede
  FROM Imp
  WHERE Ruolo = 'Programmatore')
GROUP BY Sede;