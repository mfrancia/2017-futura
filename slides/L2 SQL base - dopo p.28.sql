# ---------------------------------------------------------------------- #
# DML: WORKING ON THE DATABASE                                           #
# ---------------------------------------------------------------------- #
use mydb;

select i.nome, i.sede, s.citta
from imp i, sedi s
where i.sede = s.sede and i.ruolo = 'Programmatore';

select *
from imp i, sedi s
where i.sede = s.sede and i.ruolo = 'Programmatore';

select i.sede as SedeE001, s.sede as AltraSede
from imp i, sedi s
where i.sede <> s.sede and i.codimp = 'E001';

select g1.genitore as Nonno
from famiglia g1, famiglia g2
where g1.figlio = g2.genitore and g2.figlio = 'Anna';

select i.nome, i.sede, s.citta
from imp i, sedi s
where i.sede = s.sede;

select i.nome, i.sede, s.citta
from imp i inner join sedi s on i.sede = s.sede;

-- Aggiungi l'impiegato prima di eseguire la query
insert into imp values ('E100', 'Qui', 'Quo', 'Qua', 900);
insert into imp values ('E101', 'X', NULL, 'Y', 900);
-- La sede dell'impiegato E100 non esiste nella tabella sedi,
-- E100 farà parte dell'inner join tra le tabelle Imp e Sedi?
-- Del left (outer) join? E del right (outer) join?
select i.nome, i.sede, s.citta 
from imp i left join sedi s on i.sede = s.sede;

-- ... This is a weird join clause
select i.nome, i.sede, s.citta 
from imp i left join sedi s on i.sede = 'S03';

-- Aggiungi la sede prima di eseguire la query
insert into sedi values ('S05', 'Matteo', 'Cesena');
select i.nome, i.sede, s.citta 
from imp i right join sedi s on i.sede = s.sede;

-- FULL OUTER JOIN IS NOT AVAILABLE IN MYSQL
-- select i.nome, i.sede, s.citta
-- from imp i full outer join sedi s on i.sede = s.sede;
select i.nome, i.sede, s.citta 
from imp i left join sedi s on i.sede = s.sede
union
select i.nome, i.sede, s.citta 
from imp i right join sedi s on i.sede = s.sede;

select 'a', 'b', strcmp('a', 'b')
union
select 'b', 'b', strcmp('b', 'b')
union
select 'c', 'b', strcmp('c', 'b');

# ---------------------------------------------------------------------- #
# DDL: SCHEMA DEFINITION / UPDATE                                        #
# ---------------------------------------------------------------------- #
insert into sedi(sede, responsabile, citta) values ('SO4', 'Bruni', 'Firenze');
insert into sedi(sede, citta) values ('SO4', 'Firenze');

drop table if exists SediBologna; 
create table SediBologna (
    SedeBO char(3),
    Responsabile varchar(20) not null
);
insert into sedibologna(sedebo, responsabile)
    select sede, responsabile
    from sedi
    where citta = 'Bologna';

-- set SQL_SAFE_UPDATES = 0;
-- Delete elimini riga per riga, `truncate table tableName` invece elimina
-- tutto il contenuto in una sola volta
delete from sedi where citta = 'Bologna';

update sedi
set responsabile = 'Bruni', citta = 'Firenze'
where sede = 'S01';

update imp
set stipendio = 1.1 * stipendio
where ruolo = 'Programmatore';

-- this shouldn't work, isn't it?
insert into impwithnull values ('barz', 'foo', null, 'foobar', -9999);

alter table sedibologna rename sedibo;

alter table sedibo add livello int;

alter table sedibo drop column livello;

alter table sedibo change responsabile direttore char(10);
